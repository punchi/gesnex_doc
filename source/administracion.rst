Administración
========================================

Roles
--------------------

Los roles son perfiles que administra la empresa para definir las funciones que puede o no puede realizar los diferentes usuarios que interactúan en ella.

Identificación
^^^^^^^^^^^^^^^^
Primero, ir al menu e identificar `Roles <https://app.gesnex.com/roles>`_

.. image:: menu_roles.png
   :alt: menu roles
   
Modificación
^^^^^^^^^^^^^^^^

Luego identificar el rol (o perfil) creado para el usuario que se desea modificar y hacer click en el primer ícono.

Se desplegará una lista de funciones, marcadas con un ticket, las que puede realizar o el casillero vacío las que no puede realizar. Al hacer click a una funcion cambia el estado, luego de tener el perfil a gusto, debe ir al final o al principio de la página y hacer click en "Editar"