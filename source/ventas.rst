Ventas
===========

Devoluciones
--------------------

Las devoluciones son productos que han sido devueltos a la empresa, los que pueden acabar en el stock de donde salieron o darlos como pérdida.

Identificación
^^^^^^^^^^^^^^^^
Para crear una nueva devolución, debe primero identificarla en las `Ventas <https://app.gesnex.com/ventas>`_, y normalmente le resultará más sencillo haciendo click en "Búsqueda avanzada"

.. image:: busqueda.png
   :alt: busqueda avanzada

Una vez encontrada la venta en donde se desea realizar la devolución, hacer click en el ícono de devolución   
   
.. image:: busqueda_encontrada.png
   :alt: busqueda encontrada

Creación
^^^^^^^^^^^^^^^^
En esta ventana, usted debera indicar la cantidad de productos que ingresan a stock (se suman a la misma bodega donde salió) o si el producto se da por perdido. Opcionalmente puede escribir un comentario para futuras referencias. El sistema calculará automaticamente el dinero que debe devolver. Para finalizar, haga click en "Guardar"

.. image:: nueva_devolucion.png
   :alt: nueva devolucion