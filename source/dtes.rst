DTE's
===========

Anulaciones
--------------------

.. Note:: Página del SII revisada en mayo del 2019.

A veces se hace necesario tener que anular uno o más documentos. Esta es una manera de hacerlo con el SII.
   
- Ingresar con *certificado digital*.
- Visitar la página https://palena.sii.cl/cvc_cgi/dte/af_anular1 

  + Que es lo mismo que dirigirse a https://www.sii.cl e ir a *Servicios Online* -> *Factura electrónica* -> *Sistema de facturación de mercado* -> *Timbraje electrónico* -> *Anular folios*

- Ingresar el RUT de la empresa y el tipo de documento a anular.
- Debe identificar en que tramo se encuentra el folio o rango que desea anular.

.. image:: anulacion.png
   :alt: anulacion folios

- Una vez seleccionado el rango, finalmente ingresa los rangos a anular, junto con el motivo que podría ser como ejemplo, debido al vencimiento del CAF.

.. image:: anulacion_final.png
   :alt: anulacion confirmar