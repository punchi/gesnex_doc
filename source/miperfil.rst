Mi Perfil
===========

Esta opción se encuentra ubicada al costado superior derecho del software, al hacer click en su nombre.

Mi Cuenta
--------------------

Aquí podra cambiar la información personal de su usuario, asi como de su perfil como nombre de usuario y contraseña

Empresa
--------------------

Integración
^^^^^^^^^^^

Se cuenta con 2 integraciones listas para ser utilizadas dentro de Gesnex, una es con la aplicación |link_centry| y la otra, |link_shopify|.

.. |link_centry| raw:: html

   <a href="https://www.centry.cl" target="_blank">Centry</a>
   
.. |link_shopify| raw:: html

   <a href="https://es.shopify.com" target="_blank">Shopify</a>

Con Centry todos los productos que compartan el mismo SKU quedarán sincronizados en la cantidad de stock que Gesnex maneje, pudiendo escoger tantos productos desee sincronizar. También al momento de eliminarlo de Gesnex, puede escoger si desea desactivarlo de Centry o también eliminarlo.

Centry - Configuración
""""""""""""""""""""""""""""""""""""
Para integrarse con Centry, primero debe tener contratados sus servicios, los que puede realizar en su propio sitio web.

.. image:: activar.png
   :alt: opciones centry

Una vez cuente con una cuenta activa, debe autorizar a Gesnex para ser utilizado en Centry, por lo cual debe hacer click en el boton "Autorizar" y se desplegará una ventana como la siguiente en donde debe terminar de autorizar a Gesnex. Si no aparece esta ventana, se le solicitará iniciar sesión primero a Centry.

.. image:: autorizar.png
   :alt: autorizacion gesnex

Una vez autorizado Gesnex, se refrescará la página de Gesnex y las opciones de "Activar sincronización", junto a las otras quedarán activas para que pueda terminar de configurar. Por último debe presionar "Guardar" y todo quedará listo para su uso.

Centry - Opciones
""""""""""""""""""""""""""""""""""""
Para sincronizar un producto con Centry, el producto debe tener la opción marcada.

.. image:: sync_centry.png
   :alt: sincronizar con centry

Estas se puede setear tanto al momento de crear un producto, al modificarlo, o en la creación y edición masiva.