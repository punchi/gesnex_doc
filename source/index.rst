.. gesnex documentation master file, created by
   sphinx-quickstart on Wed Aug  9 19:37:05 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenid@ a la guía de Gesnex
========================================

La documentación está actualmente en desarrollo, si encuentra algún problema o no contiene la información que necesita, por favor comentarnos.

.. toctree::
   :caption: Gesnex
   :maxdepth: 2

   inventario
   ventas
   reportes
   administracion
   miperfil

.. toctree::
   :maxdepth: 2
   :caption: SII

   dtes